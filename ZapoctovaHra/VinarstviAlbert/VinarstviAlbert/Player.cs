﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinarstviAlbert
{
    class Player
    {
        public static int fieldCount = 1;
        public int loc_x = 0;
        public int loc_y = 0;
        public List<Items.Item> inventory = new List<Items.Item>();


        public Player(int x, int y)
        {
            loc_x = x;
            loc_y = y;
            for (int i = 0; i < 10; i++ )
            {
                inventory.Add(new Items.Seed());
            }
            inventory.Add(new Items.Hoe());
        }

        public void MovePlayer(string input)
        {
            //Console.WriteLine(direction);
            if (input == "a" && loc_x > 0)
            {
                loc_x--;
                Program.turns++;
            }
            else if (input == "d" && loc_x < Program.map.map_size_width-1)
            {
                loc_x++;
                Program.turns++;
            }
            else if (input == "w" && loc_y > 0)
            {
                loc_y--;
                Program.turns++;
            }
            else if (input == "s" && loc_y < Program.map.map_size_height-1)
            {
                loc_y++;
                Program.turns++;
            }

            //Console.Clear();
            Console.WriteLine(Program.turns);
        }

        public void PlayerAction(string input)
        {
            if (input == "2")
            {
                Console.WriteLine("planted/picked up");
                if (Program.map.field[loc_y][loc_x].plantable && HaveSeeds())
                {
                    Program.map.field[loc_y][loc_x].PlantFlower();
                    RemoveSeed();
                    Program.turns++;
                }
                else if (Program.map.field[loc_y][loc_x].flower != null && Program.map.field[loc_y][loc_x].flower.state == FlowerState.FRUIT_READY)
                {
                    Program.map.field[loc_y][loc_x].flower = null;
                    inventory.Add(new Items.Grape());
                }
                
            }
            if (input == "1")
            {
                Console.WriteLine("field processed");
                if (GetHoeEndurance() > 0 && Program.map.field[loc_y][loc_x].plantable == false && Program.map.field[loc_y][loc_x].flower != null && Program.map.field[loc_y][loc_x].flower.state == FlowerState.DEAD)
                {
                    Program.map.field[loc_y][loc_x].plantable = true;
                    Program.map.field[loc_y][loc_x].flower = null;
                    //Items.Item i = inventory.Find(x => x.GetType().ToString() == "Hoe");

                    LowerHoeEndurance();
                }
                else if (Program.map.field[loc_y][loc_x].plantable == false && Program.map.field[loc_y][loc_x].flower != null)
                {
                    
                }
                else if (GetHoeEndurance() > 0 && Program.map.field[loc_y][loc_x].plantable == false)
                {
                    Program.map.field[loc_y][loc_x].plantable = true;
                    LowerHoeEndurance();

                }

            }

            if (input == "3")
            {
                Console.WriteLine("watered");
                if (Program.map.field[loc_y][loc_x].flower != null)
                {
                    Program.map.field[loc_y][loc_x].flower.WaterFlower();
                    if (Program.map.field[loc_y][loc_x].flower.watered == 3)
                    {
                        
                        Program.map.field[loc_y][loc_x].flower.state = FlowerState.DEAD;
                    }
                }
            }
            if (input == "4")
            {
                if (Program.invBool)
                {

                    Program.invBool = false;
                }
                else
                {
                    Program.invBool = true;
                    if (Program.shopBool)
                    {
                        Program.shopBool = false;
                    }
                }


            }
            if (input == "5")
            {
                if (Program.shopBool)
                {

                    Program.shopBool = false;
                }
                else
                {
                    Program.shopBool = true;
                    if (Program.invBool)
                    {
                        Program.invBool = false;
                    }
                }

            }
            if (input == "6")
            {
                if (Program.money >= Items.Seed.price)
                {
                    for (int i = 0; i < 1; i++)
                    {
                        inventory.Add(new Items.Seed());
                        Program.money -= Items.Seed.price;
                    }
                    

                }

            }
            if (input == "7")
            {
                if (Program.money >= Items.Seed.price)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        inventory.Add(new Items.Seed());
                        Program.money -= Items.Seed.price;
                    }


                }

            }
            if (input == "8")
            {
                if (Program.money >= Items.Hoe.price)
                {
                    inventory.Add(new Items.Hoe());
                    Program.money -= Items.Hoe.price;


                }

            }
            if (input == "9")
            {
                if (GetItemCount<Items.Grape>() > 0)
                {
                    int i = Items.Item.GetFirstIndexOfType<Items.Grape>(inventory);
                    while (i >= 0)
                    {
                        Items.Grape grapeItem = (Items.Grape)inventory[i];
                        inventory.Remove(grapeItem);
                        Program.money += Items.Grape.price;
                        i = Items.Item.GetFirstIndexOfType<Items.Grape>(inventory);
                    }

                }

            }


        }

        public int GetHoeEndurance()
        {
            int i = Items.Item.GetFirstIndexOfType<Items.Hoe>(inventory);
            if (i >= 0)
            {
                Items.Hoe hoeItem = (Items.Hoe)inventory[i];
                return hoeItem.endurance;
            }
            else
            {
                return 0;
            }


        }

        public void LowerHoeEndurance()
        {

            int i = Items.Item.GetFirstIndexOfType<Items.Hoe>(inventory);
            Items.Hoe hoeItem = (Items.Hoe)inventory[i];
            if (hoeItem.endurance > 0)
            {
                hoeItem.endurance -= 1;
                if (hoeItem.endurance == 0)
                {
                    inventory.Remove(hoeItem);
                }
            }
            Console.WriteLine(i);
        }

        public int GetItemCount<T>()
        {
            int i = Items.Item.GetCountOfType<T>(inventory);
            return i;
        }

        public void RemoveSeed()
        {
            int i = Items.Item.GetFirstIndexOfType<Items.Seed>(inventory);
            if (i >= 0)
            {
                Items.Seed seedItem = (Items.Seed)inventory[i];
                inventory.Remove(seedItem);
            }

        }

        public bool HaveSeeds()
        {
            int i = Items.Item.GetFirstIndexOfType<Items.Seed>(inventory);
            if (i >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

         

    }

}
