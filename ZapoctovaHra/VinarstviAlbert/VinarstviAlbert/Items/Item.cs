﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace VinarstviAlbert.Items
{
    public class Item
    {
        public static int price;



        /*
        public static T GetFirstItemOfType <T> (List<Item> list)
        {

            Item foundItem = list.Find(i => i is T);

            return (T)foundItem;
        }

        public static T GetFirstItemOfTypeD<T>(List<Item> list)
        {
            Item foundItem = list.Find(i => i is T);

            return (T)foundItem;
        }
    }
    
    public static Hoe GetFirstHoe (List<Item> list)
    {
        Item foundItem = list.Find(x => x is Hoe);

        return foundItem;
    }

    public static int GetFirstIndexOfType(List<Item> list, string type)
    {
        Item foundItem = list.Find(x => x is Hoe);
        return list.IndexOf(foundItem);


    }
    */



        public static int GetFirstIndexOfType<T>(List<Item> list)
        {
            Item foundItem = list.Find(x => x is T);
            return list.IndexOf(foundItem);
        }


        public static int GetCountOfType<T>(List<Item> list)
        {
            List<Item> foundItems = list.FindAll(x => x is T);
            return foundItems.Count();
        }


    }
}
