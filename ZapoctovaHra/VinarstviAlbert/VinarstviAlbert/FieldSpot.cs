﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinarstviAlbert
{
    class FieldSpot
    {
        public bool plantable = false;
        public Flower flower;


        public void PlantFlower()
        {
            if (plantable)
            {
                flower = new Flower();
                flower.Planted();
                plantable = false;
            }
            
        }

    }
}
