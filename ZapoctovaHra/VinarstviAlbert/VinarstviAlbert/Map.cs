﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinarstviAlbert
{
    class Map
    {

        public List<List<FieldSpot>> field = new List<List<FieldSpot>> { };

        public int map_size_width = 10;
        public int map_size_height = 10;

        public void GenerateMap()
        {
            int row = 0;
            while (row < map_size_height)
            {
                int col = 0;

                List<FieldSpot> rowList = new List<FieldSpot> { };

                while (col < map_size_width)
                {
                    rowList.Add(new FieldSpot());
                    col++;
                }

                field.Add(rowList);
                row++;
            }
        }

        public void RemoveMap()
        {
            field = new List<List<FieldSpot>> { };
            Program.money = 50;
        }

        public void Intro()
        {
            Console.WriteLine("_________________");
            Console.WriteLine("Welcome Albert, you are owner of a small wine farm.");
            Console.WriteLine("It is difficult for us farmers to make a living these days, so good luck to you my friend.");
            Console.WriteLine("_________________");

        }
        public void RenderMap(Player player)
        {
            Console.WriteLine("");
            int y = 0;
            foreach (List<FieldSpot> fsList in field)
            {
                string row = "";
                int x = 0;

                foreach (FieldSpot fs in fsList)
                {
                    if (player.loc_x == x && player.loc_y == y)
                    {
                        row += "P";
                    }
                    else if (fs.plantable == true && fs.flower == null)
                    {
                        row += "_";
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.PLANTED)
                    {
                        row += "-";
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.GROWING)
                    {
                        row += "i";
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.GROWN)
                    {
                        row += "I";
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.FRUIT_READY)
                    {
                        row += "O";
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.DEAD)
                    {
                        row += "x";
                    }
                    else
                    {
                        row += ".";
                    }
                    x++;
                }
                y++;
                Console.WriteLine(row);
            }
            

        }

        public void PlantFlower()
        {
            field[4][5].PlantFlower();
            field[4][5].flower.Planted();
        }


        public void NextTurnChanges()
        {


            foreach (List<FieldSpot> fsList in field)
            {
                foreach (FieldSpot fs in fsList)
                {
                    if (fs.flower != null)
                    {
                        fs.flower.NextTurn();
                    }
                }


            }

        }


        public void RenderUI()
        {
            
            Console.WriteLine("_________________");

            Console.WriteLine("$ " + Program.money + "       current field: " + GetCurrentFieldState() + "       hoe endurance: " + Program.player.GetHoeEndurance());
            Console.WriteLine("\nControls:");
            Console.WriteLine("Plough    Plant/Pick Up    Water    Inventory    Shop");
            Console.WriteLine("1         2                3        4            5");
            Console.WriteLine("\nFlower states:");
            Console.WriteLine("Plantable    Planted    Growing    Fruit ready    Dead");
            Console.WriteLine("_            -          i, I       O              x");


            Console.WriteLine("_________________");
        }

        public void RenderInv()
        {

            Console.WriteLine("_________________");

            Console.WriteLine("Inventory");
            Console.WriteLine("$ " + Program.money + "\n");
            Console.WriteLine("Seeds: " + Program.player.GetItemCount<Items.Seed>());
            Console.WriteLine("Grapes: " + Program.player.GetItemCount<Items.Grape>());
            Console.WriteLine("Hoe: " + Program.player.GetItemCount<Items.Hoe>());
            Console.WriteLine("\n");
            Console.WriteLine("");


            Console.WriteLine("_________________");
        }

        public void RenderShop()
        {

            Console.WriteLine("_________________");

            Console.WriteLine("Shop");
            Console.WriteLine("$ " + Program.money + "\nBuy");
            Console.WriteLine("5$ Seed    50$ 10 Seeds    50$ Hoe");
            Console.WriteLine("6          7               8\n");
            Console.WriteLine("Sell all grapes(6$/1)");
            Console.WriteLine("9");
            Console.WriteLine("");


            Console.WriteLine("_________________");
        }

        public void WinScreen()
        {
            Console.Clear();
            Console.WriteLine("_________________");
            Console.WriteLine("You have won by collecting $" + Program.money);
            Console.WriteLine("_________________");


        }
        public void LoseScreen()
        {
            Console.Clear();
            Console.WriteLine("_________________");
            Console.WriteLine("You have lost, all your flowers died and you have not enough money or seeds to get more money");
            Console.WriteLine("_________________");


        }


        public bool IsAnyFlowerGrowing()
        {
            bool isAlive = false;
            foreach (List<FieldSpot> fsList in field)
            {
                int x = 0;

                foreach (FieldSpot fs in fsList)
                {
                    if (fs.plantable == true && fs.flower == null)
                    {
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.PLANTED)
                    {
                        isAlive = true;
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.GROWING)
                    {
                        isAlive = true;
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.GROWN)
                    {
                        isAlive = true;
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.FRUIT_READY)
                    {
                        isAlive = true;
                    }
                    else if (fs.flower != null && fs.flower.state == FlowerState.DEAD)
                    {
                        
                    }
                    else
                    {
                    }
                    x++;
                }
            }
            return isAlive;
        }


            public string GetCurrentFieldState()
        {
            if (Program.map.field[Program.player.loc_y][Program.player.loc_x].plantable)
            {
                return "plantable";
            }
            else if (Program.map.field[Program.player.loc_y][Program.player.loc_x].plantable == false && Program.map.field[Program.player.loc_y][Program.player.loc_x].flower == null)
            {
                return "empty";
            }
            else if (Program.map.field[Program.player.loc_y][Program.player.loc_x].plantable == false && Program.map.field[Program.player.loc_y][Program.player.loc_x].flower != null && Program.map.field[Program.player.loc_y][Program.player.loc_x].flower.state == FlowerState.DEAD)
            {

                return "flower dead";
            }
            else if (Program.map.field[Program.player.loc_y][Program.player.loc_x].plantable == false && Program.map.field[Program.player.loc_y][Program.player.loc_x].flower != null && Program.map.field[Program.player.loc_y][Program.player.loc_x].flower.state == FlowerState.FRUIT_READY)
            {
                return "Grapes ready";
            }
            else if (Program.map.field[Program.player.loc_y][Program.player.loc_x].flower != null)
            {
                return "growing";
            }
            else
                return "empty";
            }
        }


}



