﻿using System;
using System.Reflection;

namespace VinarstviAlbert
{
    class Program
    {
        public static Map map = new Map();
        public static Player player;
        public static int turns = 0;
        public static bool canMove = true;
        public static int money = 50;
        public static int winMoney = 1000;
        public static bool invBool = false;
        public static bool shopBool = false;
        static void Main()
        {
            Console.WindowHeight = 22;
            IntroScreen();
            map.GenerateMap();
            AddPlayer(8,8);
            map.RenderMap(player);
            map.RenderUI();
            WaitForInput();
           
        }

        static void IntroScreen()
        {
            int i = 5;
            while (i > 0)
            {
                map.Intro();
                Console.WriteLine("Press any key " + i + " times");
                string inputT = Console.ReadKey().KeyChar.ToString().ToLower();
                i--;
                Console.Clear();
            }

        }

        static void WaitForInput()
        {
            while (canMove && money < winMoney)
            {
                if (money < 5 && player.GetItemCount<Items.Seed>() == 0 && player.GetItemCount<Items.Grape>() == 0 && map.IsAnyFlowerGrowing() == false)
                {
                    Console.WriteLine("You have lost");
                    int i = 5;
                    while (i > 0)
                    {
                        map.LoseScreen();
                        Console.WriteLine("Press any key " + i + " times");
                        string inputT = Console.ReadKey().KeyChar.ToString().ToLower();
                        i--;
                        Console.Clear();
                    }
                    RemovePlayer();
                    map.RemoveMap();
                    Main();
                }
                string input = Console.ReadKey().KeyChar.ToString().ToLower();
                if (input == "w" || input == "s" || input == "a" || input == "d")
                {
                    player.MovePlayer(input);
                }
                else
                {
                    player.PlayerAction(input);
                }
                map.NextTurnChanges();
                map.RenderMap(player);
                if (invBool && shopBool == false)
                {
                    map.RenderInv();
                }
                else if (!invBool && shopBool)
                {
                    map.RenderShop();
                }
                else
                {
                    map.RenderUI();
                }
            }

            map.WinScreen();

        }

        static void AddPlayer(int x, int y)
        {
            player = new Player(x, y);
        }
        static void RemovePlayer()
        {
            player = null;
        }


    }

}
