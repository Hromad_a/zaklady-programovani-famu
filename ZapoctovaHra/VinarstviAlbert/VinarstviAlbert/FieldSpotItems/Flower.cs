﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VinarstviAlbert
{
    enum FlowerState
    {
        PLANTED,
        GROWING,
        GROWN,
        FRUIT_READY,
        DEAD
    }

    class Flower
    {
        public FlowerState state = FlowerState.PLANTED;
        public int startTurn;
        public int currentTurn;
        public int life;
        public int lifeStage;
        public int lifeStageDuration = 8;
        public int watered = 0;
     
        public Flower()
        {
            startTurn = Program.turns;
        }
        
        public void Planted ()
        {
            state = FlowerState.PLANTED;
            lifeStageDuration = 8;
        }

        public void NextTurn()
        {
            currentTurn = Program.turns;
            life = currentTurn - startTurn;
            if (lifeStage < lifeStageDuration)
            {
                lifeStage++;
            }
            else
            {
                lifeStage = 0;
                NextStage();
            }
        }

        public void NextStage()
        {
            if (state == FlowerState.PLANTED)
            {
                state = FlowerState.GROWING;
            }
            else if (state == FlowerState.GROWING)
            {
                state = FlowerState.GROWN;
            }
            else if (state == FlowerState.GROWN && watered > 0 && watered < 3)
            {
                state = FlowerState.FRUIT_READY;
                lifeStageDuration = 12;
            }
            else if (state == FlowerState.FRUIT_READY)
            {
                state = FlowerState.DEAD;
                lifeStageDuration = 8;
            }

        }

        public void WaterFlower()
        {
            watered++;
        }
    }
}
