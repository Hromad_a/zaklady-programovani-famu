﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ParsingTest
{
    class Program
    {
        public static List<List<Field>> fields = new List<List<Field>>();
        static void Main(string[] args)
        {
            Console.WriteLine("START");
            Funkce.ReadMapFile();
            Funkce.TextToMap();
            Funkce.RenderMap();
        }
    }


    class Funkce
    {

        static string mapFile = Directory.GetCurrentDirectory() + "\\map.txt";
        public static IEnumerable<string> lines;
        public static void ReadMapFile()
        {
            if (File.Exists(mapFile))
            {
                // Read all the content in one string
                // and display the string
                lines = File.ReadLines(mapFile);
                
                foreach (string line in lines)
                {
                    //Console.WriteLine(line);
                }
            }
            
        }

        public static void TextToMap()
        {
            int y = 0;
            while (lines.Count() > y)
            {

                List<Field> fieldRow = new List<Field> { };
                string lineChars = lines.ElementAt(y);
                int x = 0;
                
                while (lineChars.Count() > x)
                {
                    Field field = new Field();
                    field.ftype = GetCharToType(lineChars.ElementAt(x).ToString());
                    fieldRow.Add(field);
                    
                    x++;
                }
                Program.fields.Add(fieldRow);

                y++;
            }

        }

        public static void RenderMap()
        {
            int y = 0;
            foreach (List<Field> fRow in Program.fields)
            {
                string row = "";
                int x = 0;

                foreach (Field field in fRow)
                {
                    if (field.ftype == Field.fieldType.wall)
                    {
                        row += "X";
                    }
                    else
                    {
                        row += "_";

                    }
                    x++;
                }
                y++;
                Console.WriteLine(row);
            }

        }

        public static Field.fieldType GetCharToType(string character)
        {

            if (character == "X")
            {
                return Field.fieldType.wall;
            }
            else
            {
                return Field.fieldType.floor;
            }
        }
    }
}
