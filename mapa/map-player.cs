﻿using System;
using System.Timers;
using System.Runtime.InteropServices;


namespace Lesson_02
{
    class map_player
    {

        public static void Main()
        {
            Mapagen(0, 0);
        }

        static void Mapagen(int score, int deaths)
        {
            //Console.Clear();
            int player_x = 0;
            int player_y = 9;
            int map_width = 15;
            int map_height = 15;
            int door_x = 0;
            int door_y = 0;
            int enemy_x = 0;
            int enemy_y = 0;
            int projectile_x = -5;
            int projectile_y = -5;
            bool projectile = false;
            bool isRunning = true;
            bool firstRun = true;
            bool direction = true;
            bool keepDirection = false;
            bool shootingDirection = direction;
            int moveEnemy = 2;
            bool enemyAlive = true;

            if (firstRun)
            {
                player_x = RandomIntBetween(0, map_width - 1);
                player_y = RandomIntBetween(0, map_height - 1);
                door_x = RandomIntBetween(0, map_width - 1);
                door_y = RandomIntBetween(0, map_height - 1);
                enemy_x = RandomIntBetween(0, map_width - 1);
                enemy_y = RandomIntBetween(0, map_height - 1);

                while (player_x == door_x && player_y == door_y || player_x == enemy_x && player_y == enemy_y || door_x == enemy_x && door_y == enemy_y)
                {
                    door_x = RandomIntBetween(0, map_width - 1);
                    door_y = RandomIntBetween(0, map_height - 1);
                    enemy_x = RandomIntBetween(0, map_width - 1);
                    enemy_y = RandomIntBetween(0, map_height - 1);
                }

                firstRun = false;

            }

            while (isRunning)
            {
                DrawMap(map_width, map_height, player_x, player_y, door_x, door_y, enemy_x, enemy_y, direction, projectile_x, projectile_y, projectile, enemyAlive);

                Console.WriteLine("wsad = pohyb na mapě");
                Console.WriteLine("f = střelba projektilu");
                Console.WriteLine("r = náhodná poloha hráče na mapě");
                Console.WriteLine("\nUteč před nepřítelem(*) a dostaň se do dveří(/)");
                Console.WriteLine("\nSkóre: " + score);
                Console.WriteLine("\nPočet smrtí: " + deaths);


                //Move Enemy
                if (moveEnemy % 2 == 0)
                {
                    enemy_x = MoveEnemyX(enemy_x, player_x);
                    enemy_y = MoveEnemyY(enemy_y, player_y);
                    moveEnemy++;
                }
                else
                {
                    moveEnemy++;
                }

                //Player movement
                switch (MovePlayer().ToLower().Trim())
                {
                    case "w":
                        if (player_y > 0)
                        {
                            player_y -= 1;
                            Console.WriteLine("Pohyb na sever");
                        }
                        break;
                    case "s":
                        if (player_y < map_height - 1)
                        {
                            player_y += 1;
                            Console.WriteLine("Pohyb na jih");
                        }
                        break;
                    case "d":
                        if (player_x < map_width - 1)
                        {
                            player_x += 1;
                            Console.WriteLine("Pohyb na východ");
                            direction = true;
                        }
                        break;
                    case "a":
                        if (player_x > 0)
                        {
                            player_x -= 1;
                            Console.WriteLine("Pohyb na západ");
                            direction = false;
                        }
                        break;
                    case "r":
                        if (true)
                        {
                            player_x = RandomIntBetween(0, map_width - 1);
                            player_y = RandomIntBetween(0, map_height - 1);
                            Console.WriteLine("Náhodná poloha hráče");
                        }
                        break;
                    case "f":
                        if (CanShoot(projectile_x, map_width))
                        {
                            projectile_x = player_x;
                            projectile_y = player_y;
                            projectile = true;
                            keepDirection = true;
                            shootingDirection = direction;
                            Console.WriteLine("Náboj vystřelen");
                        }
                        break;
                    default:
                        Console.WriteLine("Neplatný znak");
                        break;

                }

                //Move Projectile
                if(projectile)
                {
                    if(shootingDirection)
                    {
                        projectile_x++;
                        keepDirection = true;
                    }
                    else 
                    {
                        projectile_x--;
                        keepDirection = true;
                    }

                }
                else
                {
                    keepDirection = false;
                }



                //Console.Clear();
                if(CheckProjectileCol(projectile_x, projectile_y, enemy_x, enemy_y))
                {
                    enemyAlive = false;
                }

                if (CheckEnemyCol(player_x, player_y, enemy_x, enemy_y, enemyAlive))
                {
                    deaths++;
                    GameOver(deaths, score);
                }
                if (player_x == door_x && player_y == door_y)
                {
                    firstRun = true;
                    score++;
                    Mapagen(score, deaths);
                }


                //Console.Clear();
            }
        }

        static void DrawMap(int width, int height, int playerX, int playerY, int doorX, int doorY, int enemy_x, int enemy_y, bool direction, int projectile_x, int projectile_y, bool projectile, bool enemyAlive)
        {
            string mapa = "";
            int renderX = 0;
            int renderY = 0;
            while (renderY < height)
            {
                renderX = 0;
                while (renderX < width)
                {
                    if (renderX == playerX && renderY == playerY)
                    {
                        if(direction)
                        {
                            mapa += "> ";
                        }
                        else
                        {
                            mapa += "< ";
                        }
                    }
                    else if (renderX == doorX && renderY == doorY)
                    {
                        mapa += "/ ";
                    }
                    else if (renderX == enemy_x && renderY == enemy_y && enemyAlive)
                    {
                        mapa += "* ";
                    }
                    else if(renderX == projectile_x && renderY == projectile_y)
                    {
                        if(projectile)
                        mapa += "0 ";
                    }
                    else
                    {
                        mapa += ". ";
                    }
                    renderX += 1;

                }
                mapa += "\n";
                renderY += 1;

            }
            Console.WriteLine(mapa);
        }

        static string MovePlayer()
        {
            string direction = Console.ReadKey().KeyChar.ToString();
            //Console.WriteLine(direction);
            return direction;
        }


        static int MoveEnemyX(int enemy_x, int player_x)
        {
            if (player_x > enemy_x)
            {
                enemy_x++;
            }
            else if (player_x < enemy_x)
            {
                enemy_x--;
            }
            else
            {

            }
            return enemy_x;
        }
        static int MoveEnemyY(int enemy_y, int player_y)
        {
            if (player_y > enemy_y)
            {
                enemy_y++;
            }
            else if (player_y < enemy_y)
            {
                enemy_y--;
            }
            else
            {

            }
            return enemy_y;
        }


        static int RandomIntBetween(int min, int max)
        {
            var rand = new Random();
            return rand.Next(min, max);
        }

        static bool CheckEnemyCol(int player_x, int player_y, int enemy_x, int enemy_y, bool enemyAlive)
        {
            if (Math.Abs(player_x - enemy_x) <= 1 && Math.Abs(player_y - enemy_y) <= 0 || Math.Abs(player_x - enemy_x) <= 0 && Math.Abs(player_y - enemy_y) <= 1)
            {
                if(enemyAlive)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }
        static bool CheckProjectileCol(int projectile_x, int projectile_y, int enemy_x, int enemy_y)
        {
            if (projectile_x == enemy_x && projectile_y == enemy_y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        static void GameOver( int deaths, int score)
        {
            Console.WriteLine("Prohrál jsi, nepřítel tě dostal.");
            Console.WriteLine("Počet smrtí: " + deaths);
            Console.WriteLine("Score: " + score);
            Console.WriteLine("Hrát znovu?");
            Console.ReadLine();
            Mapagen(score, deaths);
        }

        static bool CanShoot(int projectile_x, int map_width)
        {
            if(projectile_x > map_width || projectile_x < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}