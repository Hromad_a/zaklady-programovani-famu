﻿using System;
using System.Timers;

namespace Lesson_01
{
    class Program
    {

        static void test()
        {
            /*

            Console.WriteLine("Napiste slovo");

            string input = Console.ReadLine();

            if(input.Length > 10)
            {

                Console.WriteLine("Moc dlouhe slovo");
            }
            else
            {
                Console.WriteLine("Vse ok");
            }

            Console.WriteLine("Napiste cislo");

            string input2 = Console.ReadLine();

            */

        }

        static void Second()
        {
            int vysledek;
            int number1 = int.Parse(Console.ReadLine());
            int number2 = int.Parse(Console.ReadLine());
            vysledek = number1 + number2;

            if (number1 + number2 > 100)
            {

                Console.WriteLine("cislo je dostatecne vysoke");

            }
            Console.WriteLine("soucet je " + vysledek);

            if (vysledek > 50 && vysledek < 150)
            {
                Console.WriteLine("soucet je akorát velký");

            }
            else if (vysledek < 50 || vysledek > 150)
            {

                Console.WriteLine("soucet je moc malý nebo moc velký");
            }
            Forth();

        }


        static void Forth()
        {
            Console.WriteLine(" \nHadej cislo");

            int finalNum = 10;
            int inputNum = int.Parse(Console.ReadLine());
            while (inputNum != finalNum)
            {
                Console.WriteLine("Hledej dal");
                inputNum = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Konecne jsi nasel to spravne cislo");


            Forth();

        }



        static void Mapagen()
        {
            int player_x = 0;
            int player_y = 9;
            int map_width = 10;
            int map_height = 10;
            int door_x = 0;
            int door_y = 0;
            int enemy_x = 0;
            int enemy_y = 0;
            bool isRunning = true;
            bool firstRun = true;
            int moveEnemy = 2;
            int score = 0;
            int deaths = 0;

            if(firstRun)
            {
                player_x = RandomIntBetween(0, map_width-1);
                player_y = RandomIntBetween(0, map_height-1);
                door_x = RandomIntBetween(0, map_width-1);
                door_y = RandomIntBetween(0, map_height-1);
                enemy_x = RandomIntBetween(0, map_width - 1);
                enemy_y = RandomIntBetween(0, map_height - 1);

                while (player_x == door_x && player_y == door_y && player_x == enemy_x && player_y == enemy_y && door_x == enemy_x && door_y == enemy_y)
                {
                    door_x = RandomIntBetween(0, map_width - 1);
                    door_y = RandomIntBetween(0, map_height - 1);
                    enemy_x = RandomIntBetween(0, map_width - 1);
                    enemy_y = RandomIntBetween(0, map_height - 1);
                }

                firstRun = false;

            }

            while (isRunning)
            {
                DrawMap(map_width, map_height, player_x, player_y, door_x, door_y, enemy_x, enemy_y);
            
            Console.WriteLine("wsad = pohyb na mapě");
            Console.WriteLine("r = náhodná poloha hráče na mapě");
            Console.WriteLine("\nUteč před nepřítelem(w) a dostaň se do dveří(/)");
            Console.WriteLine("\nSkóre: " + score);
            Console.WriteLine("\nPočet smrtí: " + deaths);


                //Move Enemy
                if (moveEnemy % 2 == 0)
                {
                    enemy_x = MoveEnemyX(enemy_x, player_x);
                    enemy_y = MoveEnemyY(enemy_y, player_y);
                    moveEnemy++;
                }
                else
                {
                    moveEnemy++;
                }

                switch (MovePlayer().ToLower().Trim())
                {
                case "w":
                    if (player_y > 0)
                    {
                        player_y -= 1;
                        Console.WriteLine("Pohyb na sever");
                    }
                    break; 
                case "s":
                    if (player_y < map_height-1)
                    {
                        player_y += 1;
                        Console.WriteLine("Pohyb na jih");
                    }
                    break;
                case "d":
                    if (player_x < map_width-1)
                    {
                        player_x += 1;
                        Console.WriteLine("Pohyb na východ");

                    }
                    break;
                case "a":
                    if (player_x > 0)
                    {
                        player_x -= 1;
                        Console.WriteLine("Pohyb na západ");
                    }
                    break;
                    case "r":
                        if (true)
                        {
                            player_x = RandomIntBetween(0, map_width-1);
                            player_y = RandomIntBetween(0, map_height - 1);
                            Console.WriteLine("Náhodná poloha hráče");
                        }
                        break;
                    default:
                    Console.WriteLine("Neplatný znak");
                    break;

            }
                Console.Clear();
                if(CheckEnemyCol(player_x, player_y, enemy_x, enemy_y))
                {
                    GameOver();
                }
                if(player_x == door_x && player_y == door_y)
                {
                    firstRun = true;
                    Mapagen();
                }
            }
        }

        static void DrawMap(int width, int height, int playerX, int playerY, int doorX, int doorY, int enemy_x, int enemy_y)
        {
            string mapa = "";
            int renderX = 0;
            int renderY = 0;
            while(renderY < height)
            {
                renderX = 0;
                while(renderX < width)
                {
                    if ( renderX == playerX && renderY == playerY)
                    {
                        mapa += "-";
                    }
                    else if(renderX == doorX && renderY == doorY)
                    {
                        mapa += "/";
                    }
                    else if (renderX == enemy_x && renderY == enemy_y)
                    {
                        mapa += "Y";
                    }
                    else
                    {
                        mapa += "O";
                    }
                    renderX += 1;

                }
                mapa += "\n";
                renderY += 1;

            }
            Console.WriteLine(mapa);
        }

        static string MovePlayer()
        {
            string direction = Console.ReadKey().KeyChar.ToString();
            return direction;
        }

        static int MoveEnemyX(int enemy_x, int player_x)
        {
            if(player_x > enemy_x)
            {
                enemy_x++;
            }
            else if(player_x < enemy_x)
            {
                enemy_x--;
            }
            else
            {

            }
            return enemy_x;
        }
        static int MoveEnemyY(int enemy_y, int player_y)
        {
            if (player_y > enemy_y)
            {
                enemy_y++;
            }
            else if (player_y < enemy_y)
            {
                enemy_y--;
            }
            else
            {

            }
            return enemy_y;
        }


        static int RandomIntBetween(int min, int max)
        {
            var rand = new Random();
            return rand.Next(min, max);
        }

        static bool CheckEnemyCol(int player_x, int player_y, int enemy_x, int enemy_y)
        {
            if (Math.Abs(player_x - enemy_x) <= 1 && Math.Abs(player_y - enemy_y) <= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void GameOver()
        {
            Console.WriteLine("Prohrál jsi, nepřítel tě dostal.");
            Console.WriteLine("Hrát znovu?");
            Console.ReadLine();
            Mapagen();
        }

    }
}